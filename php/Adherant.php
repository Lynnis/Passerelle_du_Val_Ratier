<?php

require_once 'config.php';   //lien avec la page de connexion a la BDD

error_reporting(0);

session_start();

if (isset($_SESSION['username'])) {
    header("Location: index.php");
}

if (isset($_POST['submit'])) {
    $user = $_POST['nom'];
    $prenom = $_POST['prenom'];      //information qui vont être recus dans la BDD
    $email = $_POST['email'];
    $telephone = $_POST['telephone'];
    $Motivation = $_POST['Motivation'];

    if ($email == $email) {
        $sql = "SELECT * FROM benevole WHERE email='$email'";
        $result = mysqli_query($conn, $sql);
        if (!$result->num_rows > 0) {
            $sql = "INSERT INTO benevole (nom, prenom, email, telephone, Motivation)         //requête
					VALUES ('$user', '$prenom', '$email', '$telephone', '$Motivation')";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                echo "<script>alert('Le team vous recontactera pour vous donner l\'accès à l\'espace adhérant après la validation de vos information.')</script>";  //Si l'envoie des données a bien été effectué
                $user = "";
                $prenom = "";
                $email = "";
                $telephone = "";
                $Motivation = "";
            } else {
                echo "<script>alert('Oups ! Une erreur est survenue')</script>";    //Si l'envoie des données a echouer
            }
        } else {
            echo "<script>alert('Oups ! cette email est déja utiliser)</script>";       // si l'email n'existe pas
        }

    } else {
        echo "<script>alert('Mot de passe incorrecte')</script>";        //Si le mot de passe est incorrecte
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="../css/login.css" rel="stylesheet">

    <title>Register Form - Pure Coding</title>
</head>
<body>
<div class="foreground"></div>

<div class="midground">
    <div class="tuna"></div>
</div>

<div class="background">
</div>
<div class="container">
    <form action="" method="POST" class="login-email">
        <p class="login-text" style="font-size: 2rem; font-weight: 800;">Je souhaite adhéré et/ou être bénévole</p>
        <div class="input-group">
            <input type="text" placeholder="nom" name="nom" value="<?php echo $nom; ?>" required>
        </div>
        <div class="input-group">
            <input type="prenom" placeholder="prenom" name="prenom" value="<?php echo $prenom; ?>" required>
        </div>
        <div class="input-group">
            <input type="email" placeholder="email" name="email" value="<?php echo $email; ?>" required>
        </div>
        <div class="input-group">
            <input type="telephone" placeholder="telephone" name="telephone" value="<?php echo $telephone; ?>" required>
        </div>
        <div class="input-group">
            <input type="Motivation" placeholder="Motivation" name="Motivation" value="<?php echo $Motivation; ?>" required>
        </div>
        <div class="input-group">
            <button name="submit" class="btn">Envoie de mes données</button>
        </div>
    <center><a href="../html/Acceuil.html" title="Ceci est un lien image HTML">
            <img
                    src="../img/home.png"                                                      //retour acceuil
                    width="60"
                    height="50"
        </a> </center>

</div>
</body>
</html>